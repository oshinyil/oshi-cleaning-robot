﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OshiCleaningRobot.Core;
using OshiCleaningRobot.Models;

namespace OshiCleaningRobot.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/CleaningRobot")]
    public class CleaningRobotController : Controller
    {
        private readonly IProcessor _processor;
        private readonly ILogger _logger;

        public CleaningRobotController(IProcessor processor, ILoggerFactory loggerFactory)
        {
            _processor = processor;
            _logger = loggerFactory.CreateLogger<CleaningRobotController>();
        }

        [HttpGet]
        public string Get()
        {
            return "Welcome to Cleaning Robot App";
        }

        [HttpPost]
        public IActionResult Post([FromBody]Request request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            try
            {
                var result = _processor.Process(request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(500);
            }
        }
    }
}