﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OshiCleaningRobot.Core;
using OshiCleaningRobot.Core.Instructions;

namespace OshiCleaningRobot.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IProcessor, Processor>();
            services.AddTransient<IInstruction, TurnLeftInstruction>();
            services.AddTransient<IInstruction, TurnRightInstruction>();
            services.AddTransient<IInstruction, AdvanceInstruction>();
            services.AddTransient<IInstruction, BackInstruction>();
            services.AddTransient<IInstruction, CleanInstruction>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
