﻿using OshiCleaningRobot.Core.Instructions;
using OshiCleaningRobot.Models;
using System.Collections.Generic;
using Xunit;

namespace OshiCleaningRobot.Core.Tests
{
    public class CleanInstructionTest
    {
        private readonly IInstruction _instructionHandler;

        public CleanInstructionTest()
        {
            _instructionHandler = new CleanInstruction();
        }

        [Fact]
        public void Code_ReturnValidCode()
        {
            var code = _instructionHandler.Code;

            Assert.Equal(Constant.Instruction.Clean, code);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(4)]
        public void IsBatteryEnough_ReturnFalse_GivenBatteryLessThanFive(int value)
        {
            var isBatteryEnough = _instructionHandler.IsBatteryEnough(value);

            Assert.False(isBatteryEnough);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(6)]
        public void IsBatteryEnough_ReturnTrue_GivenBatteryMoreThanEqualsFive(int value)
        {
            var isBatteryEnough = _instructionHandler.IsBatteryEnough(value);

            Assert.True(isBatteryEnough);
        }

        [Fact]
        public void Do_ReturnTrue()
        {
            var map = new string[,] { { "S", "S" } };
            var currentState = new Result
            {
                Visited = new List<Point>(),
                Cleaned = new List<Point>(),
                Final = new Position
                {
                    X = 0,
                    Y = 0,
                    Facing = Constant.Direction.North
                },
                Battery = 10
            };

            var success = _instructionHandler.Do(map, currentState);

            Assert.True(success);
            Assert.Equal(0, currentState.Final.X);
            Assert.Equal(0, currentState.Final.Y);
            Assert.Equal(Constant.Direction.North, currentState.Final.Facing);
            Assert.Equal(5, currentState.Battery);
            Assert.Empty(currentState.Visited);
            Assert.Contains(currentState.Cleaned, p => p.X == currentState.Final.X && p.Y == currentState.Final.Y);
        }
    }
}
