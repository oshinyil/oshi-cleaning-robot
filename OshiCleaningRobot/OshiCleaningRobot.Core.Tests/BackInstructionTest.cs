﻿using OshiCleaningRobot.Core.Instructions;
using OshiCleaningRobot.Models;
using System.Collections.Generic;
using Xunit;

namespace OshiCleaningRobot.Core.Tests
{
    public class BackInstructionTest
    {
        private readonly IInstruction _instructionHandler;

        public BackInstructionTest()
        {
            _instructionHandler = new BackInstruction();
        }

        [Fact]
        public void Code_ReturnValidCode()
        {
            var code = _instructionHandler.Code;

            Assert.Equal(Constant.Instruction.Back, code);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(2)]
        public void IsBatteryEnough_ReturnFalse_GivenBatteryLessThanThree(int value)
        {
            var isBatteryEnough = _instructionHandler.IsBatteryEnough(value);

            Assert.False(isBatteryEnough);
        }

        [Theory]
        [InlineData(3)]
        [InlineData(4)]
        public void IsBatteryEnough_ReturnTrue_GivenBatteryMoreThanEqualsThree(int value)
        {
            var isBatteryEnough = _instructionHandler.IsBatteryEnough(value);

            Assert.True(isBatteryEnough);
        }

        [Fact]
        public void Do_ReturnTrue_FacingNorth()
        {
            var map = new string[,] { { "S" }, { "S" } };
            var currentState = new Result
            {
                Visited = new List<Point>(),
                Cleaned = new List<Point>(),
                Final = new Position
                {
                    X = 0,
                    Y = 0,
                    Facing = Constant.Direction.North
                },
                Battery = 10
            };

            var success = _instructionHandler.Do(map, currentState);

            Assert.True(success);
            Assert.Equal(0, currentState.Final.X);
            Assert.Equal(1, currentState.Final.Y);
            Assert.Equal(Constant.Direction.North, currentState.Final.Facing);
            Assert.Equal(7, currentState.Battery);
            Assert.Contains(currentState.Visited, p => p.X == currentState.Final.X && p.Y == currentState.Final.Y);
            Assert.Empty(currentState.Cleaned);
        }

        [Fact]
        public void Do_ReturnTrue_FacingEast()
        {
            var map = new string[,] { { "S", "S" } };
            var currentState = new Result
            {
                Visited = new List<Point>(),
                Cleaned = new List<Point>(),
                Final = new Position
                {
                    X = 1,
                    Y = 0,
                    Facing = Constant.Direction.East
                },
                Battery = 10
            };

            var success = _instructionHandler.Do(map, currentState);

            Assert.True(success);
            Assert.Equal(0, currentState.Final.X);
            Assert.Equal(0, currentState.Final.Y);
            Assert.Equal(Constant.Direction.East, currentState.Final.Facing);
            Assert.Equal(7, currentState.Battery);
            Assert.Contains(currentState.Visited, p => p.X == currentState.Final.X && p.Y == currentState.Final.Y);
            Assert.Empty(currentState.Cleaned);
        }

        [Fact]
        public void Do_ReturnTrue_FacingSouth()
        {
            var map = new string[,] { { "S" }, { "S" } };
            var currentState = new Result
            {
                Visited = new List<Point>(),
                Cleaned = new List<Point>(),
                Final = new Position
                {
                    X = 0,
                    Y = 1,
                    Facing = Constant.Direction.South
                },
                Battery = 10
            };

            var success = _instructionHandler.Do(map, currentState);

            Assert.True(success);
            Assert.Equal(0, currentState.Final.X);
            Assert.Equal(0, currentState.Final.Y);
            Assert.Equal(Constant.Direction.South, currentState.Final.Facing);
            Assert.Equal(7, currentState.Battery);
            Assert.Contains(currentState.Visited, p => p.X == currentState.Final.X && p.Y == currentState.Final.Y);
            Assert.Empty(currentState.Cleaned);
        }

        [Fact]
        public void Do_ReturnTrue_FacingWest()
        {
            var map = new string[,] { { "S", "S" } };
            var currentState = new Result
            {
                Visited = new List<Point>(),
                Cleaned = new List<Point>(),
                Final = new Position
                {
                    X = 0,
                    Y = 0,
                    Facing = Constant.Direction.West
                },
                Battery = 10
            };

            var success = _instructionHandler.Do(map, currentState);

            Assert.True(success);
            Assert.Equal(1, currentState.Final.X);
            Assert.Equal(0, currentState.Final.Y);
            Assert.Equal(Constant.Direction.West, currentState.Final.Facing);
            Assert.Equal(7, currentState.Battery);
            Assert.Contains(currentState.Visited, p => p.X == currentState.Final.X && p.Y == currentState.Final.Y);
            Assert.Empty(currentState.Cleaned);
        }

        [Theory]
        [MemberData(nameof(ObstacleData))]
        public void Do_ReturnFalse_GivenObstacle(string[,] map, Result currentState)
        {
            var initialX = currentState.Final.X;
            var initialY = currentState.Final.Y;
            var initialFacing = currentState.Final.Facing;

            var success = _instructionHandler.Do(map, currentState);

            Assert.False(success);
            Assert.Equal(initialX, currentState.Final.X);
            Assert.Equal(initialY, currentState.Final.Y);
            Assert.Equal(initialFacing, currentState.Final.Facing);
            Assert.Equal(7, currentState.Battery);
            Assert.Empty(currentState.Visited);
            Assert.Empty(currentState.Cleaned);
        }

        public static IEnumerable<object[]> ObstacleData
        {
            get
            {
                var objectTests = new List<object[]>();

                var cantBeOccupiedCase = new object[]
                {
                    new string[,] { { "C" }, { "S" } },
                    new Result
                    {
                        Visited = new List<Point>(),
                        Cleaned = new List<Point>(),
                        Final = new Position
                        {
                            X = 0,
                            Y = 1,
                            Facing = Constant.Direction.South
                        },
                        Battery = 10
                    }
                };

                var wallCase = new object[]
                {
                    new string[,] { { "null" }, { "S" } },
                    new Result
                    {
                        Visited = new List<Point>(),
                        Cleaned = new List<Point>(),
                        Final = new Position
                        {
                            X = 0,
                            Y = 1,
                            Facing = Constant.Direction.South
                        },
                        Battery = 10
                    }
                };

                var outOfIndexCase = new object[]
                {
                    new string[,] { { "S" } },
                    new Result
                    {
                        Visited = new List<Point>(),
                        Cleaned = new List<Point>(),
                        Final = new Position
                        {
                            X = 0,
                            Y = 0,
                            Facing = Constant.Direction.South
                        },
                        Battery = 10
                    }
                };

                objectTests.Add(cantBeOccupiedCase);
                objectTests.Add(wallCase);
                objectTests.Add(outOfIndexCase);

                return objectTests;
            }
        }
    }
}
