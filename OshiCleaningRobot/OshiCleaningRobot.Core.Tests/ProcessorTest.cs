﻿using OshiCleaningRobot.Core.Instructions;
using OshiCleaningRobot.Models;
using System.Collections.Generic;
using Xunit;

namespace OshiCleaningRobot.Core.Tests
{
    public class ProcessorTest
    {
        private readonly IProcessor _processor;

        public ProcessorTest()
        {
            var instructions = new List<IInstruction>
            {
                new TurnLeftInstruction(),
                new TurnRightInstruction(),
                new AdvanceInstruction(),
                new BackInstruction(),
                new CleanInstruction()
            };

            _processor = new Processor(instructions);
        }

        [Fact]
        public void Process_ReturnValidResult_GivenEnoughBattery()
        {
            var request = new Request
            {
                Map = new string[,]
                {
                    { "S", "S", "S", "S" },
                    { "S", "S", "C", "S" },
                    { "S", "S", "S", "S" },
                    { "S", "null", "S", "S" }
                },
                Start = new Position
                {
                    X = 3,
                    Y = 1,
                    Facing = Constant.Direction.South
                },
                Commands = new string[]
                {
                    Constant.Instruction.TurnRight, Constant.Instruction.Advance, Constant.Instruction.Clean, Constant.Instruction.Advance,
                    Constant.Instruction.Clean, Constant.Instruction.TurnRight, Constant.Instruction.Advance, Constant.Instruction.Clean
                },
                Battery = 1094
            };

            var result = _processor.Process(request);

            Assert.Equal(4, result.Visited.Count);
            Assert.Contains(result.Visited, p => p.X == 2 && p.Y == 2);
            Assert.Contains(result.Visited, p => p.X == 3 && p.Y == 0);
            Assert.Contains(result.Visited, p => p.X == 3 && p.Y == 1);
            Assert.Contains(result.Visited, p => p.X == 3 && p.Y == 2);
            Assert.Equal(3, result.Cleaned.Count);
            Assert.Contains(result.Cleaned, p => p.X == 2 && p.Y == 2);
            Assert.Contains(result.Cleaned, p => p.X == 3 && p.Y == 0);
            Assert.Contains(result.Cleaned, p => p.X == 3 && p.Y == 2);
            Assert.Equal(3, result.Final.X);
            Assert.Equal(2, result.Final.Y);
            Assert.Equal(Constant.Direction.East, result.Final.Facing);
            Assert.Equal(1040, result.Battery);
        }

        [Fact]
        public void Process_ReturnValidResult_GivenEmptyBattery()
        {
            var request = new Request
            {
                Map = new string[,]
                {
                    { "S", "S", "S", "S" },
                    { "S", "S", "C", "S" },
                    { "S", "S", "S", "S" },
                    { "S", "null", "S", "S" }
                },
                Start = new Position
                {
                    X = 3,
                    Y = 1,
                    Facing = Constant.Direction.South
                },
                Commands = new string[]
                {
                    Constant.Instruction.TurnRight, Constant.Instruction.Advance, Constant.Instruction.Clean, Constant.Instruction.Advance,
                    Constant.Instruction.Clean, Constant.Instruction.TurnRight, Constant.Instruction.Advance, Constant.Instruction.Clean
                },
                Battery = 0
            };

            var result = _processor.Process(request);

            Assert.Equal(1, result.Visited.Count);
            Assert.Contains(result.Visited, p => p.X == 3 && p.Y == 1);
            Assert.Empty(result.Cleaned);
            Assert.Equal(3, result.Final.X);
            Assert.Equal(1, result.Final.Y);
            Assert.Equal(Constant.Direction.South, result.Final.Facing);
            Assert.Equal(0, result.Battery);
        }
    }
}
