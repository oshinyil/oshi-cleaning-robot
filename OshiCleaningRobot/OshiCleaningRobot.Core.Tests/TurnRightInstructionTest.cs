﻿using OshiCleaningRobot.Core.Instructions;
using OshiCleaningRobot.Models;
using System.Collections.Generic;
using Xunit;

namespace OshiCleaningRobot.Core.Tests
{
    public class TurnRightInstructionTest
    {
        private readonly IInstruction _instructionHandler;

        public TurnRightInstructionTest()
        {
            _instructionHandler = new TurnRightInstruction();
        }

        [Fact]
        public void Code_ReturnValidCode()
        {
            var code = _instructionHandler.Code;

            Assert.Equal(Constant.Instruction.TurnRight, code);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void IsBatteryEnough_ReturnFalse_GivenBatteryLessThanOne(int value)
        {
            var isBatteryEnough = _instructionHandler.IsBatteryEnough(value);

            Assert.False(isBatteryEnough);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void IsBatteryEnough_ReturnTrue_GivenBatteryMoreThanEqualsOne(int value)
        {
            var isBatteryEnough = _instructionHandler.IsBatteryEnough(value);

            Assert.True(isBatteryEnough);
        }

        [Fact]
        public void Do_FacingEast_GivenStateFacingNorth()
        {
            var map = new string[1, 1];
            var currentState = new Result
            {
                Visited = new List<Point>(),
                Cleaned = new List<Point>(),
                Final = new Position
                {
                    X = 0,
                    Y = 0,
                    Facing = Constant.Direction.North
                },
                Battery = 10
            };

            var success = _instructionHandler.Do(map, currentState);

            Assert.True(success);
            Assert.Equal(0, currentState.Final.X);
            Assert.Equal(0, currentState.Final.Y);
            Assert.Equal(Constant.Direction.East, currentState.Final.Facing);
            Assert.Equal(9, currentState.Battery);
            Assert.Empty(currentState.Visited);
            Assert.Empty(currentState.Cleaned);
        }

        [Fact]
        public void Do_FacingSouth_GivenStateFacingEast()
        {
            var map = new string[1, 1];
            var currentState = new Result
            {
                Visited = new List<Point>(),
                Cleaned = new List<Point>(),
                Final = new Position
                {
                    X = 0,
                    Y = 0,
                    Facing = Constant.Direction.East
                },
                Battery = 10
            };

            var success = _instructionHandler.Do(map, currentState);

            Assert.True(success);
            Assert.Equal(0, currentState.Final.X);
            Assert.Equal(0, currentState.Final.Y);
            Assert.Equal(Constant.Direction.South, currentState.Final.Facing);
            Assert.Equal(9, currentState.Battery);
            Assert.Empty(currentState.Visited);
            Assert.Empty(currentState.Cleaned);
        }

        [Fact]
        public void Do_FacingWest_GivenStateFacingSouth()
        {
            var map = new string[1, 1];
            var currentState = new Result
            {
                Visited = new List<Point>(),
                Cleaned = new List<Point>(),
                Final = new Position
                {
                    X = 0,
                    Y = 0,
                    Facing = Constant.Direction.South
                },
                Battery = 10
            };

            var success = _instructionHandler.Do(map, currentState);

            Assert.True(success);
            Assert.Equal(0, currentState.Final.X);
            Assert.Equal(0, currentState.Final.Y);
            Assert.Equal(Constant.Direction.West, currentState.Final.Facing);
            Assert.Equal(9, currentState.Battery);
            Assert.Empty(currentState.Visited);
            Assert.Empty(currentState.Cleaned);
        }

        [Fact]
        public void Do_FacingNorth_GivenStateFacingWest()
        {
            var map = new string[1, 1];
            var currentState = new Result
            {
                Visited = new List<Point>(),
                Cleaned = new List<Point>(),
                Final = new Position
                {
                    X = 0,
                    Y = 0,
                    Facing = Constant.Direction.West
                },
                Battery = 10
            };

            var success = _instructionHandler.Do(map, currentState);

            Assert.True(success);
            Assert.Equal(0, currentState.Final.X);
            Assert.Equal(0, currentState.Final.Y);
            Assert.Equal(Constant.Direction.North, currentState.Final.Facing);
            Assert.Equal(9, currentState.Battery);
            Assert.Empty(currentState.Visited);
            Assert.Empty(currentState.Cleaned);
        }
    }
}
