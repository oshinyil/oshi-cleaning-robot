﻿using OshiCleaningRobot.Models;

namespace OshiCleaningRobot.Core.Instructions
{
    public class TurnLeftInstruction : InstructionBase
    {
        public override string Code => Constant.Instruction.TurnLeft;

        protected override int ConsumableBatteryUnit => 1;

        public override bool Do(string[,] map, Result currentState)
        {
            switch (currentState.Final.Facing)
            {
                case Constant.Direction.North:
                    currentState.Final.Facing = Constant.Direction.West;
                    break;
                case Constant.Direction.East:
                    currentState.Final.Facing = Constant.Direction.North;
                    break;
                case Constant.Direction.South:
                    currentState.Final.Facing = Constant.Direction.East;
                    break;
                case Constant.Direction.West:
                    currentState.Final.Facing = Constant.Direction.South;
                    break;
                default:
                    break;
            }

            currentState.Battery = ConsumeBattery(currentState.Battery);
            return true;
        }
    }
}
