﻿using System.Linq;
using OshiCleaningRobot.Models;

namespace OshiCleaningRobot.Core.Instructions
{
    public class CleanInstruction : InstructionBase
    {
        public override string Code => Constant.Instruction.Clean;

        protected override int ConsumableBatteryUnit => 5;

        public override bool Do(string[,] map, Result currentState)
        {
            if (!currentState.Cleaned.Any(p => p.X == currentState.Final.X && p.Y == currentState.Final.Y))
            {
                currentState.Cleaned.Add(new Point
                {
                    X = currentState.Final.X,
                    Y = currentState.Final.Y
                });
            }

            currentState.Battery = ConsumeBattery(currentState.Battery);

            return true;
        }
    }
}
