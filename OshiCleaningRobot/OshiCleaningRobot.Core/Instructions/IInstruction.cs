﻿using OshiCleaningRobot.Models;

namespace OshiCleaningRobot.Core.Instructions
{
    public interface IInstruction
    {
        string Code { get; }
        bool IsBatteryEnough(int currentBatteryUnit);
        bool Do(string[,] map, Result currentState);
    }
}
