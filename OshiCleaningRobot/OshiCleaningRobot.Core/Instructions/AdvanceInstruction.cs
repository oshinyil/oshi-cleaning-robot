﻿using System.Linq;
using OshiCleaningRobot.Models;

namespace OshiCleaningRobot.Core.Instructions
{
    public class AdvanceInstruction : InstructionBase
    {
        public override string Code => Constant.Instruction.Advance;

        protected override int ConsumableBatteryUnit => 2;

        public override bool Do(string[,] map, Result currentState)
        {
            var newPosition = Move(currentState.Final.X, currentState.Final.Y, currentState.Final.Facing);
            currentState.Battery = ConsumeBattery(currentState.Battery);

            if (IsWallOrObstacle(map, newPosition.X, newPosition.Y))
            {
                return false;
            }

            if (!currentState.Visited.Any(p => p.X == newPosition.X && p.Y == newPosition.Y))
            {
                currentState.Visited.Add(new Point
                {
                    X = newPosition.X,
                    Y = newPosition.Y
                });
            }

            currentState.Final = newPosition;
            return true;
        }

        private Position Move(int x, int y, char facing)
        {
            var newPosition = new Position
            {
                X = x,
                Y = y,
                Facing = facing
            };

            switch (facing)
            {
                case Constant.Direction.North:
                    newPosition.Y -= 1;
                    break;
                case Constant.Direction.East:
                    newPosition.X += 1;
                    break;
                case Constant.Direction.South:
                    newPosition.Y += 1;
                    break;
                case Constant.Direction.West:
                    newPosition.X -= 1;
                    break;
                default:
                    break;
            }

            return newPosition;
        }
    }
}
