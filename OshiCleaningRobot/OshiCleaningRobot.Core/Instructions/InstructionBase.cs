﻿using OshiCleaningRobot.Models;

namespace OshiCleaningRobot.Core.Instructions
{
    public abstract class InstructionBase : IInstruction
    {
        protected abstract int ConsumableBatteryUnit { get; }

        protected int ConsumeBattery(int currentBatteryUnit)
        {
            return currentBatteryUnit - ConsumableBatteryUnit;
        }

        protected bool IsWallOrObstacle(string[,] map, int x, int y)
        {
            var xLength = map.GetLength(1);
            var yLength = map.GetLength(0);

            return (x < 0) 
                || (x >= xLength) 
                || (y < 0) 
                || (y >= yLength) 
                || (map[y, x] == Constant.Null
                || (map[y, x] == Constant.CantBeOccupied));
        }

        #region IInstruction Implementation

        public bool IsBatteryEnough(int currentBatteryUnit)
        {
            return currentBatteryUnit - ConsumableBatteryUnit >= 0;
        }

        public abstract string Code { get; }

        public abstract bool Do(string[,] map, Result currentState);

        #endregion
    }
}
