﻿namespace OshiCleaningRobot.Core
{
    public static class Constant
    {
        public const string Null = "null";
        public const string CantBeOccupied = "C";

        public static class Direction
        {
            public const char North = 'N';
            public const char East = 'E';
            public const char South = 'S';
            public const char West = 'W';
        }

        public static class Instruction
        {
            public const string TurnLeft = "TL";
            public const string TurnRight = "TR";
            public const string Advance = "A";
            public const string Back = "B";
            public const string Clean = "C";
        }
    }
}
