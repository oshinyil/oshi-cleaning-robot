﻿using OshiCleaningRobot.Models;

namespace OshiCleaningRobot.Core
{
    public interface IProcessor
    {
        Result Process(Request request);
    }
}
