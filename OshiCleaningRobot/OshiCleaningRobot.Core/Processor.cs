﻿using System.Collections.Generic;
using System.Linq;
using OshiCleaningRobot.Core.Instructions;
using OshiCleaningRobot.Models;

namespace OshiCleaningRobot.Core
{
    public class Processor : IProcessor
    {
        private readonly Dictionary<string, IInstruction> _instructionHandlers;

        private static readonly IList<string>[] _backofStrategies =
        {
            new List<string>{ Constant.Instruction.TurnRight, Constant.Instruction.Advance },
            new List<string>{ Constant.Instruction.TurnLeft, Constant.Instruction.Back, Constant.Instruction.TurnRight, Constant.Instruction.Advance },
            new List<string>{ Constant.Instruction.TurnLeft, Constant.Instruction.TurnLeft, Constant.Instruction.Advance },
            new List<string>{ Constant.Instruction.TurnRight, Constant.Instruction.Back, Constant.Instruction.TurnRight, Constant.Instruction.Advance },
            new List<string>{ Constant.Instruction.TurnLeft, Constant.Instruction.TurnLeft, Constant.Instruction.Advance }
        };

        public Processor(IEnumerable<IInstruction> instructions)
        {
            _instructionHandlers = new Dictionary<string, IInstruction>();
            PopulateInstructionHandlerDictionary(instructions);
        }

        public Result Process(Request request)
        {
            var currentState = new Result
            {
                Visited = new List<Point>
                {
                    new Point { X = request.Start.X, Y = request.Start.Y}
                },
                Cleaned = new List<Point>(),
                Final = request.Start,
                Battery = request.Battery
            };

            // run all commands
            foreach (var command in request.Commands)
            {
                if (IsBatteryEmpty(currentState.Battery))
                {
                    break;
                }

                var handler = _instructionHandlers[command];
                if (handler.IsBatteryEnough(currentState.Battery) && handler.Do(request.Map, currentState))
                {
                    continue;
                }
                else if (RunBackOfStrategies(request.Map, currentState))
                {
                    continue;
                }
                else
                {
                    break;
                }
            }

            currentState.Visited = currentState.Visited.OrderBy(p => p.X).ThenBy(p => p.Y).ToList();
            currentState.Cleaned = currentState.Cleaned.OrderBy(p => p.X).ThenBy(p => p.Y).ToList();

            return currentState;
        }

        private void PopulateInstructionHandlerDictionary(IEnumerable<IInstruction> instructions)
        {
            foreach (var instruction in instructions)
            {
                _instructionHandlers.Add(instruction.Code, instruction);
            }
        }

        private bool RunBackOfStrategies(string[,] map, Result currentState)
        {
            // loop through all strategies
            foreach (var strategy in _backofStrategies)
            {
                var isSuccess = false;

                // run each command in strategy
                foreach (var command in strategy)
                {
                    if (IsBatteryEmpty(currentState.Battery))
                    {
                        return false;
                    }

                    var handler = _instructionHandlers[command];
                    isSuccess = handler.IsBatteryEnough(currentState.Battery) && handler.Do(map, currentState);

                    if (isSuccess)
                    {
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                if (isSuccess)
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsBatteryEmpty(int batteryUnit)
        {
            return batteryUnit <= 0;
        }
    }
}
