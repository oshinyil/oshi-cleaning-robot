﻿using Newtonsoft.Json;

namespace OshiCleaningRobot.Models
{
    public class Position : Point
    {
        [JsonProperty(PropertyName = "facing")]
        public char Facing { get; set; }
    }
}
