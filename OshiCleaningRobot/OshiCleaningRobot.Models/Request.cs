﻿using Newtonsoft.Json;

namespace OshiCleaningRobot.Models
{
    public class Request
    {
        [JsonProperty(PropertyName = "map")]
        public string[,] Map { get; set; }

        [JsonProperty(PropertyName = "start")]
        public Position Start { get; set; }

        [JsonProperty(PropertyName = "commands")]
        public string[] Commands { get; set; }

        [JsonProperty(PropertyName = "battery")]
        public int Battery { get; set; }
    }
}
