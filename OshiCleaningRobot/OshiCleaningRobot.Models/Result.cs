﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OshiCleaningRobot.Models
{
    public class Result
    {
        [JsonProperty(PropertyName = "visited")]
        public IList<Point> Visited { get; set; }

        [JsonProperty(PropertyName = "cleaned")]
        public IList<Point> Cleaned { get; set; }

        [JsonProperty(PropertyName = "final")]
        public Position Final { get; set; }

        [JsonProperty(PropertyName = "battery")]
        public int Battery { get; set; }
    }
}
