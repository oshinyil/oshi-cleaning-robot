﻿using Microsoft.Extensions.DependencyInjection;
using OshiCleaningRobot.Core;
using OshiCleaningRobot.Core.Instructions;
using System;

namespace OshiCleaningRobot.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // create service collection
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            // create service provider
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // entry to run app
            serviceProvider.GetService<App>().Run(args);
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            // add services
            serviceCollection.AddTransient<IProcessor, Processor>();
            serviceCollection.AddTransient<IInstruction, TurnLeftInstruction>();
            serviceCollection.AddTransient<IInstruction, TurnRightInstruction>();
            serviceCollection.AddTransient<IInstruction, AdvanceInstruction>();
            serviceCollection.AddTransient<IInstruction, BackInstruction>();
            serviceCollection.AddTransient<IInstruction, CleanInstruction>();

            // add app
            serviceCollection.AddTransient<App>();
        }
    }
}
