﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

namespace OshiCleaningRobot.ConsoleApp
{
    public static class Utils
    {
        public static T ReadJsonObjectFromFile<T>(string fileName) 
            where T : class
        {
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException($"Couldn't find file: {fileName}", fileName);
            }

            var jsonString = File.ReadAllText(fileName);
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public static void WriteJsonObjectToFile<T>(T jsonObject, string fileName)
            where T : class
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return;
            }

            var jsonString = JsonConvert.SerializeObject(jsonObject, Formatting.Indented);
            File.WriteAllText(fileName, jsonString);
        }

        public static string[] SplitCommandLine(string commandLine)
        {
            var translatedArguments = new StringBuilder(commandLine);
            var escaped = false;
            for (var i = 0; i < translatedArguments.Length; i++)
            {
                if (translatedArguments[i] == '"')
                {
                    escaped = !escaped;
                }
                if (translatedArguments[i] == ' ' && !escaped)
                {
                    translatedArguments[i] = '\n';
                }
            }

            var toReturn = translatedArguments.ToString().Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < toReturn.Length; i++)
            {
                toReturn[i] = RemoveMatchingQuotes(toReturn[i]);
            }

            return toReturn;
        }

        private static string RemoveMatchingQuotes(string stringToTrim)
        {
            var firstQuoteIndex = stringToTrim.IndexOf('"');
            var lastQuoteIndex = stringToTrim.LastIndexOf('"');
            while (firstQuoteIndex != lastQuoteIndex)
            {
                stringToTrim = stringToTrim.Remove(firstQuoteIndex, 1);
                stringToTrim = stringToTrim.Remove(lastQuoteIndex - 1, 1);
                firstQuoteIndex = stringToTrim.IndexOf('"');
                lastQuoteIndex = stringToTrim.LastIndexOf('"');
            }

            return stringToTrim;
        }
    }
}
