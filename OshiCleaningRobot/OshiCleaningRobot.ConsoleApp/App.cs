﻿using OshiCleaningRobot.Core;
using OshiCleaningRobot.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace OshiCleaningRobot.ConsoleApp
{
    public class App
    {
        // AutoResetEvent to signal when to exit the application.
        private static readonly AutoResetEvent waitHandle = new AutoResetEvent(false);

        private readonly IProcessor _processor;

        public App(IProcessor processor)
        {
            _processor = processor;
        }

        public void Run(string[] args)
        {
            // Fire and forget
            Task.Run(() =>
            {
                Console.WriteLine("Welcome to Cleaning Robot App.");
                while (true)
                {
                    var input = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(input))
                    {
                        Console.WriteLine($"Unknown command.");
                        continue;
                    }

                    var inputArgs = Utils.SplitCommandLine(input);

                    var command = inputArgs[0];
                    if (command != "cleaning_robot")
                    {
                        Console.WriteLine($"Unknown command: {command}.");
                        continue;
                    }

                    if (inputArgs.Length < 2 || string.IsNullOrWhiteSpace(inputArgs[1]))
                    {
                        Console.WriteLine("Please enter input json file argument.");
                        continue;
                    }

                    if (inputArgs.Length < 3 || string.IsNullOrWhiteSpace(inputArgs[2]))
                    {
                        Console.WriteLine("Please enter result json file argument.");
                        continue;
                    }

                    try
                    {
                        var request = Utils.ReadJsonObjectFromFile<Request>(inputArgs[1]);
                        var result = _processor.Process(request);
                        Utils.WriteJsonObjectToFile(result, inputArgs[2]);

                        Console.WriteLine("Process completed.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Process failed. Error message: {ex.Message}");
                    }
                }
            });

            // Handle Control+C or Control+Break
            Console.CancelKeyPress += (o, e) =>
            {
                Console.WriteLine("Exit.");

                // Exit application
                Environment.Exit(0);
            };

            // Wait
            waitHandle.WaitOne();
        }
    }
}
