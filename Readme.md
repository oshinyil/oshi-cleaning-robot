# Cleaning Robot Application

## Solution Overview

This application is developed using .NET Core. Here is the solution structure:

* `OshiCleaningRobot.ConsoleApp` : The console app
* `OshiCleaningRobot.WebApi` : The Web API app (REST)
* `OshiCleaningRobot.Models` : Contains all domain models
* `OshiCleaningRobot.Core` : The core business logic for this app
* `OshiCleaningRobot.Core.Tests` : Unit test for OshiCleaningRobot.Core

We will talk more about `OshiCleaningRobot.Core`. This is the core business logic for the cleaning robot app. This project contains two important interfaces.

1. `IProcessor`. This interface has `Process` method that will be called by client app to process the given `Request` object. The method will process the request based on the defined algorithm and then return `Result` object. It is up to the client wheter this object will be serialized as file or returned to the API caller as JSON.
   
   `Processor` is a class which implements `IProcessor`. It has a collection of instruction handlers to process instructions. It also has set of back of strategies that will be run if the instruction hits an obstacle.


2. `IInstruction`. This interface has set of methods to do instruction.

   * `Code` method returns a code to identify what instruction is handled by this object. For example: `TL`, `TR`, `A`, `C`, and `B`.
   * `IsBateryEnough` method returns whether the given battery unit is enough to perform a instruction.
   * `Do` method performs the instruction. It returns whether the instruction is done successfully. It also modifies the given state.

   There is `InstructionBase` abstract class that implements `IInstruction` interface. It provides default behaviours for its derived class.
   
   If there is a new instruction to implement in the future, we just need to create a class that inherits from `InstructionBase`, and implement the abstract methods. And then, we also need to register this newley created class to the `CollectionServices` so that it can be detected by `Processor` automatically using dependency injection.


## Prerequisites
Before building the solution, You also need to have Docker with Linux container installed, up and running.

## Build Solution with Docker

You can build the Web API and Console app in Docker using the following commands. The instructions assume that you are in the root of the repository.

```
cd OshiCleaningRobot
docker-compose build
```

## Run Console App with Docker

First, you need to create a directory for sharing between Docker and host machine. In this example, I use `c:/cleaning-robot` for sharing directory, and then map it to `/data` in the container. You will also need to put your JSON file into this sharing directory. Then, you can run the console app in Docker using the following commands. 

```
 docker run -v c:/cleaning-robot:/data -i --name console-app cleaningrobot-consoleapp
```

This brings you to the interactive console. You can type command after a message `Welcome to Cleaning Robot App.` is displayed. For example:

```
cleaning_robot "/data/test1.json" "/data/test1_result.json"
```

You can press CTRL+C to exit the application.

## Run Web API with Docker

You can run the Web API in Docker using the following commands. 

```
docker run -d -p 5000:80 --name web-api cleaningrobot-webapi
```

The Web API will run on localhost port 5000. You can make `POST` request to `/api/CleaningRobot` path using any REST client tool. Below is an example how to send the request.

```
POST http://localhost:5000/api/CleaningRobot

Content-Type: application/json

{
  "map": [
    ["S", "S", "S", "S"],
    ["S", "S", "C", "S"],
    ["S", "S", "S", "S"],
    ["S", "null", "S", "S"]
  ],
  "start": {"X": 3, "Y": 0, "facing": "N"},
  "commands": [ "TL","A","C","A","C","TR","A","C"],
  "battery": 80
}

```